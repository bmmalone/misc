This repo contains many python3 scripts and helpers that I find useful. 

**DEPRECATED**

This package is now deprecated. Please use either
[`pymisc-utils`](https://github.com/bmmalone/pymisc-utils) or
[`pybio-utils`](https://github.com/bmmalone/pybio-utils).

**Install**

This package is written in python3. pip can be used to install it:

``pip3 install .``

(The "period" at the end is required.)

If possible, I recommend installing inside a virtual environment. See [here](http://www.simononsoftware.com/virtualenv-tutorial-part-2/>), for example.

**BLAS**


Additionally, a BLAS library, such as [OpenBLAS](http://www.openblas.net/), is used for efficiency. This library is installed by default on many versions of Unix. It is also available through many standard package managers. For example:

* Ubuntu: ``sudo apt-get install libblas-dev liblapack-dev libatlas-base-dev gfortran``
* CentOS: ``sudo yum install atlas-devel lapack-devel blas-devel libgfortran``

If the use of package managers are not an option (for example, because they require root access) or just not desired, then OpenBLAS can be installed locally. The following instructions have been tested extensively on Ubuntu, but they may require modification for other distributions.

For OSX, a custom installation of numpy linking against the OpenBLAS library is required. We do not officially support this, but [example documentation](http://dedupe.readthedocs.io/en/latest/OSX-Install-Notes.html) is available elsewhere on the web.

N.B. In principle, any BLAS and ATLAS library, such as [Intel's Math Kernel Library](https://software.intel.com/en-us/intel-mkl), can be used. This has not been tested and is not supported, though.


```python
# download the OpenBLAS source
git clone https://github.com/xianyi/OpenBLAS
    
# compile the library
cd OpenBLAS && make FC=gfortran

### Lines to add to .bashrc
# for the OpenBLAS library
export LD_LIBRARY_PATH=/path/to/OpenBLAS:$LD_LIBRARY_PATH
export BLAS=/path/to/libopenblas.a
export ATLAS=/path/to/libopenblas.a
    
# then, to be safe, close the current terminal and open a new one to install the package
```

If this does not work, please consult the [scipy documentation](https://www.scipy.org/install.html).


**Usage**

The package includes many command line scripts; some are documented below.

* ``test-gzip``. This program can be used as a stress-test for file IO. It creates a random binary file, gzips it, and writes it to disk. It then calculates the SHA256 checksum, copies the gzipped file to a new files, renames the new file, and recalculates the checksum. It repeats this as many times as desired. If this checksums do not match, then an OSError is raised.

    Example call: ``test-gzip 10M --num-iterations 100 --logging-level DEBUG --read-file /data/read-me.gz --write-file /data/write-me.gz``