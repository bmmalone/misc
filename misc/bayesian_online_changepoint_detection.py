###
#
#   This script is copied almost exactly from:
#       https://github.com/hildensia/bayesian_changepoint_detection/
#
#   That, in turn, is based on code associated with a technical report:
#       Adams, R. P. & MacKay, D. J. C. Bayesian Online Changepoint Detection.
#           University of Cambridge, 2007.
#
###

import numpy as np
import scipy.misc
import scipy.stats

class OnlineChangepointDetector:
    """ This class implements the online change point detection algorithm. It
        must be instantiated with a "hazard function," which gives the likelihood
        of observing a partition of length l, and an "observation_likelihood" 
        object for calculating the likelihood of observations and which handles 
        parameter updates.

        The published examples use univariate Gaussian observations, with normal 
        (for the mean) and inverse Gamma (for the variance) priors on the 
        observation distribution. The predictive distribution is then a Student's T.
        Other conjugate-exponential families (say, Dirichlet-multinomial) should
        not be difficult to implement.
    """

    def __init__(self, hazard_function, observation_likelihood):
        """ This function constructs an OnlineChangepointDetector. The hazard
            function must be callable, and the observation_likelihood must
            implement "pdf" and "update_theta" methods.

            Args:
                self (object) : this object

                hazard_function (callable) : a function which gives the likelihood
                    of observing a partition of length r

                observation_likelihood (object) : an object with the methods
                    "pdf" (which gives the likelihood of a particular observation) 
                    and "update_theta" (which updates its hyperparameters based 
                    on a given observation).
        """
        self.hazard_function = hazard_function
        self.observation_likelihood = observation_likelihood

        self.t = 0

        # we also keep track of P(r_t) = k

        # unlike the original code, we _DO_ implement this in an online fashion,
        # so we really only keep track of the "current" estimates. External 
        # control is necessary to keep track of the history.

        self.R = np.zeros(1)
        self.R[0] = 1

        self.logR = np.zeros(1)

    def observe_datum(self, x):
        """ This method handles observing a new datum. In estimates the growth
            and changepoint probabilities. It also updates the hyperparameters
            of the observation_likelihood.

            Args:
                self (object) : this object

                x (var) : the observed datum. The observation_likelihood "pdf"
                    and "update_theta" methods must know how to work with the
                    observation.

            Returns:
                np.array (of floats) : the updated likelihoods that the current
                    partition of the data is of length l
        """


        # Evaluate the predictive distribution for the new datum under each of
        # the parameters.  This is the standard thing from Bayesian inference.
        predprobs = self.observation_likelihood.pdf(x)
        
        # Evaluate the hazard function for this interval
        H = self.hazard_function(np.array(range(self.t+1)))

        # Evaluate the growth probabilities - shift the probabilities down and to
        # the right, scaled by the hazard function and the predictive
        # probabilities.
        newR = np.zeros(self.t+2)
        newR[1:] = self.R * predprobs * (1-H)
       
        # Evaluate the probability that there *was* a changepoint and we're
        # accumulating the mass back down at r = 0.
        newR[0] = np.sum( self.R * predprobs * H)
        
        # Renormalize the run length probabilities for improved numerical
        # stability.
        self.R = newR / np.sum(newR)
        
        # Update the parameter sets for each possible run length.
        self.observation_likelihood.update_theta(x)
        
        self.t += 1

        return np.array(self.R)

    def observe_datum_log(self, x):
        """ This method handles observing a new datum. In estimates the growth
            and changepoint probabilities. It also updates the hyperparameters
            of the observation_likelihood.

            The difference between this function and observe_datum is that this
            version works in log-space to maintain numeric stability.

            Args:
                self (object) : this object

                x (var) : the observed datum. The observation_likelihood "pdf"
                    and "update_theta" methods must know how to work with the
                    observation.

            Returns:
                np.array (of floats) : the updated log likelihoods that the current
                    partition of the data is of length l
        """


        # Evaluate the predictive distribution for the new datum under each of
        # the parameters.  This is the standard thing from Bayesian inference.
        predprobs = self.observation_likelihood.logpdf(x)
        
        # Evaluate the hazard function for this interval
        H = self.hazard_function(np.array(range(self.t+1)))

        # Evaluate the growth probabilities - shift the probabilities down and to
        # the right, scaled by the hazard function and the predictive
        # probabilities.
        newR = np.zeros(self.t+2)
        #newR[1:] = self.R * predprobs * (1-H)
        newR[1:] = self.logR + predprobs + np.log(1-H)
       
        # Evaluate the probability that there *was* a changepoint and we're
        # accumulating the mass back down at r = 0.
        newR[0] = scipy.misc.logsumexp( self.logR + predprobs + np.log(H))

        # now, convert out of log-space back to [0,1]
        m = np.max(newR)
        newR = newR - m
        prob = np.exp(newR)
        prob = prob / np.sum(prob)
        
        # but store the normalized probabilities back in log space
        self.logR = np.log(prob)
        
        # Update the parameter sets for each possible run length.
        self.observation_likelihood.update_theta(x)
        
        self.t += 1

        return prob


def constant_hazard(lam, r):
    """ This is a hazard function which is a discrete exponential (geometric)
        distribution with timescale lam.

        Args:
            lam (float) : the rate parameter of the exponential distribution

            r (np.array) : an array whose shape gives the number of observations

        Returns:
            np.array : the constant 1/lam array
    """
    return 1/lam * np.ones(r.shape)


class StudentT:
    """ This class implements a Student's t distribution, presumably for use with
        the OnlineChangepointDetector class. This choice is made because the
        predictive distribution of a Gaussian with normal-inverse Gamma priors is
        a Student's t.

        For more information about the derivations and calculations, see Section 3 of:
            
            Murphy, K. Conjugate Bayesian analysis of the Gaussian distribution.
                University of British Columbia, 2007.
    """

    def __init__(self, alpha, beta, kappa, mu):
        """ The constructor for the Student's t distribution, which we consider as
            a Gaussian observation distribution with normal-inverse Gamma priors.
            alpha and beta are the hyperparameters for the normal prior on the 
            observation mean, and kappa and mu and the hyperparameters for the
            inverse Gamma prior on the observation variance.

            Args:
                alpha, beta (floats) : hyperparameters on the normal prior for
                    the mean

                kappa, mu (floats) : hyperparameters on the inverse Gamma prior 
                    for the variance
        """
        self.alpha0 = self.alpha = np.array([alpha])
        self.beta0 = self.beta = np.array([beta])
        self.kappa0 = self.kappa = np.array([kappa])
        self.mu0 = self.mu = np.array([mu])

    def pdf(self, x):
        """ This method calculates the probability of observing the given data,
            based on the current hyperparameters. This function is mostly a
            wrapper around scipy.stats.t.pdf, using the appropriate parameters
            for that distribution.

            Args:
                x (float or np.array of floats) : the observation(s)

            Returns:
                float : the likelihood of the observations given 
                    the current hyperparameters
        """
        df = 2*np.abs(self.alpha)
        loc = self.mu
        scale = np.sqrt(self.beta * (self.kappa+1) / (np.abs(self.alpha) * self.kappa))
        return scipy.stats.t.pdf(x=x, 
                           df=df,
                           loc=loc,
                           scale=scale)

    
    def logpdf(self, x):
        """ This method calculates the log of the probability of observing the given data,
            based on the current hyperparameters. This function is mostly a
            wrapper around scipy.stats.t.logpdf, using the appropriate parameters
            for that distribution.

            Args:
                x (float or np.array of floats) : the observation(s)

            Returns:
                float : the likelihood of the observations given 
                    the current hyperparameters
        """
        df = 2*np.abs(self.alpha)
        loc = self.mu
        scale = np.sqrt(self.beta * (self.kappa+1) / (np.abs(self.alpha) * self.kappa))
        return scipy.stats.t.logpdf(x=x, 
                           df=df,
                           loc=loc,
                           scale=scale)


    def update_theta(self, x):
        """ This method updates the hyperparameters after observing the single 
            data point, x. See the Murphy reference for the derivation of the
            update equations.

            Args: 
                x (float) : the observation

            Returns:
                None, but the hyperparameters are updated to incorporate the
                    new observation.
        """
        muT0 = np.concatenate((self.mu0, (self.kappa * self.mu + x) / (self.kappa + 1)))
        kappaT0 = np.concatenate((self.kappa0, self.kappa + 1.))
        alphaT0 = np.concatenate((self.alpha0, self.alpha + 0.5))
        betaT0 = np.concatenate((self.beta0, self.beta + 
            (self.kappa * (x - self.mu)**2) / (2. * (self.kappa + 1.))))
            
        self.mu = muT0
        self.kappa = kappaT0
        self.alpha = alphaT0
        self.beta = betaT0
