from setuptools import find_packages, setup

bio_console_scripts = [
    'bam-to-wiggle=misc.bio_programs.bam_to_wiggle:main [bio]',
    'bedx-to-bedy=misc.bio_programs.bedx_to_bedy:main [bio]',
    'bed12-to-gtf=misc.bio_programs.bed12_to_gtf:main [bio]',
    'calculate-bed-overlap=misc.bio_programs.calculate_bed_overlap:main [bio]',
    'convert-ccds-to-bed=misc.bio_programs.convert_ccds_to_bed:main [bio]',
    'count-aligned-reads=misc.bio_programs.count_aligned_reads:main [bio]',
    'count-reads=misc.bio_programs.count_reads:main [bio]',
    'create-aligned-read-count-bar-chart=misc.bio_programs.plotting.create_aligned_read_count_bar_chart:main [bio]',
    'create-mygene-report=misc.bio_programs.create_mygene_report:main [bio]',
    'dna-to-aa=misc.bio_programs.dna_to_aa:main [bio]',
    'download-srr-files=misc.bio_programs.download_srr_files:main [bio]',
    'extract-bed-sequences=misc.bio_programs.extract_bed_sequences:main [bio]',
    'extract-cds-coordinates=misc.bio_programs.extract_cds_coordinates:main [bio]',
    'fasta-to-fastq=misc.bio_programs.fasta_to_fastq:main [bio]',
    'fastq-pe-dedupe=misc.bio_programs.fastq_pe_dedupe:main [bio]',
    'filter-bam-by-ids=misc.bio_programs.filter_bam_by_ids:main [bio]',
    'fix-all-bed-files=misc.bio_programs.fix_all_bed_files:main [bio]',
    'get-all-utrs=misc.bio_programs.get_all_utrs:main [bio]',
    'get-read-length-distribution=misc.bio_programs.get_read_length_distribution:main [bio]',
    'gtf-to-bed12=misc.bio_programs.gtf_to_bed12:main [bio]',
    'join-long-chromosomes=misc.bio_programs.join_long_chromosomes:main [bio]',
    'merge-isoforms=misc.bio_programs.merge_isoforms:main [bio]',
    'parse-meme-names=misc.bio_programs.parse_meme_names:main [bio]',
    'plot-read-length-distribution=misc.bio_programs.plotting.plot_read_length_distribution:main [bio]',
    'remove-duplicate-bed-entries=misc.bio_programs.remove_duplicate_bed_entries:main [bio]',
    'remove-duplicate-sequences=misc.bio_programs.remove_duplicate_sequences:main [bio]',
    'remove-multimapping-reads=misc.bio_programs.remove_multimapping_reads:main [bio]',
    'reorder-fasta=misc.bio_programs.reorder_fasta:main [bio]',
    'run-bowtie=misc.bio_programs.run_bowtie:main [bio]',
    'split-bed12-blocks=misc.bio_programs.split_bed12_blocks:main [bio]',
    'split-long-chromosomes=misc.bio_programs.split_long_chromosomes:main [bio]',
    'subtract-bed=misc.bio_programs.subtract_bed:main [bio]',
]

stan_console_scripts = [
    'pickle-stan=misc.pickle_stan:main [stan]'
]

other_console_scripts = [
    'visualize-roc=misc.visualize_roc:main',
    'call-sbatch=misc.call_sbatch:main',
    'scancel-range=misc.scancel_range:main',
    'test-gzip=misc.test_gzip:main',
    'call-program=misc.call_program:main'
]

console_scripts = bio_console_scripts + stan_console_scripts + other_console_scripts



def readme():
    with open('README.md') as f:
        return f.read()

setup(name='misc',
        version='0.1.6',
        description="This package contains python3 utilities I find useful.",
        long_description=readme(),
        keywords="utilities",
        url="",
        author="Brandon Malone",
        author_email="bmmalone@gmail.com",
        license='MIT',
        packages=find_packages(),
        install_requires = [
            'cython',
            'numpy',
            'scipy',
            'matplotlib',
            'pandas',
            'docopt',
            'tqdm',
            'joblib',
            'xlrd',
            'openpyxl',
            'graphviz'
        ],
        extras_require = {
            'bio': ['biopython', 'mygene'],
            'hdf5': ['tables'],
            'ssh': ['paramiko', 'spur'],
            'stan': ['pystan'],
            #'automl': ['git+https://github.com/automl/auto-sklearn.git#egg=autosklearn']
        },
        include_package_data=True,
        test_suite='nose.collector',
        tests_require=['nose'],
        entry_points = {
            'console_scripts': console_scripts
        },
        zip_safe=False
        )
